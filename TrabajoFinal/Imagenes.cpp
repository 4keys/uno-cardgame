#include "Imagenes.h"

namespace TrabajoFinal
{
	Imagenes::Imagenes()
	{
		// Interfaces
		Imagenes::Inicio = gcnew Bitmap(Image::FromFile("Imagenes\\Interfaces\\Inicio.jpg"));
		Imagenes::ReparticionCartas = gcnew Bitmap(Image::FromFile("Imagenes\\Interfaces\\ReparticionCartas.png"));
		Imagenes::FondoDeDados = gcnew Bitmap(Image::FromFile("Imagenes\\Interfaces\\FondoDeDados.png"));
		Imagenes::Campo = gcnew Bitmap(Image::FromFile("Imagenes\\Interfaces\\Campo.jpeg"));
		Imagenes::Puntuacion = gcnew Bitmap(Image::FromFile("Imagenes\\Interfaces\\Puntuacion.png"));

		// Cartas
		Imagenes::Y0 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y0.png"));
		Imagenes::Y1 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y1.png"));
		Imagenes::Y2 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y2.png"));
		Imagenes::Y3 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y3.png"));
		Imagenes::Y4 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y4.png"));
		Imagenes::Y5 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y5.png"));
		Imagenes::Y6 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y6.png"));
		Imagenes::Y7 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y7.png"));
		Imagenes::Y8 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y8.png"));
		Imagenes::Y9 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Y9.png"));
		Imagenes::Ym2 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Ym2.png"));
		Imagenes::YR = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\YR.png"));
		Imagenes::YB = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\YB.png"));
		Imagenes::B0 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B0.png"));
		Imagenes::B1 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B1.png"));
		Imagenes::B2 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B2.png"));
		Imagenes::B3 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B3.png"));
		Imagenes::B4 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B4.png"));
		Imagenes::B5 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B5.png"));
		Imagenes::B6 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B6.png"));
		Imagenes::B7 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B7.png"));
		Imagenes::B8 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B8.png"));
		Imagenes::B9 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\B9.png"));
		Imagenes::Bm2 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Bm2.png"));
		Imagenes::BR = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\BR.png"));
		Imagenes::BB = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\BB.png"));
		Imagenes::R0 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R0.png"));
		Imagenes::R1 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R1.png"));
		Imagenes::R2 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R2.png"));
		Imagenes::R3 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R3.png"));
		Imagenes::R4 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R4.png"));
		Imagenes::R5 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R5.png"));
		Imagenes::R6 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R6.png"));
		Imagenes::R7 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R7.png"));
		Imagenes::R8 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R8.png"));
		Imagenes::R9 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\R9.png"));
		Imagenes::Rm2 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Rm2.png"));
		Imagenes::RR = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\RR.png"));
		Imagenes::RB = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\RB.png"));
		Imagenes::G0 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G0.png"));
		Imagenes::G1 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G1.png"));
		Imagenes::G2 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G2.png"));
		Imagenes::G3 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G3.png"));
		Imagenes::G4 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G4.png"));
		Imagenes::G5 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G5.png"));
		Imagenes::G6 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G6.png"));
		Imagenes::G7 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G7.png"));
		Imagenes::G8 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G8.png"));
		Imagenes::G9 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\G9.png"));
		Imagenes::Gm2 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\Gm2.png"));
		Imagenes::GR = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\GR.png"));
		Imagenes::GB = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\GB.png"));
		Imagenes::m4 = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\m4.png"));
		Imagenes::cC = gcnew Bitmap(Image::FromFile("Imagenes\\Cartas\\cC.png"));
	}

}