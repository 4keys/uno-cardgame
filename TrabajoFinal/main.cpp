#include "WinForm.h"

using namespace System;
using namespace System::Windows::Forms;

namespace TrabajoFinal
{
	[STAThread]
	void main()
	{
		Application::EnableVisualStyles();
		Application::SetCompatibleTextRenderingDefault(0);
		WinForm^ form = gcnew WinForm();
		Application::Run(form);
	}	
}
