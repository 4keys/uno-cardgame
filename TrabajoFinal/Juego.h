#pragma once
#include "Escena.h"
#include "Jugador.h"

namespace TrabajoFinal
{
	public ref class Juego : public Escena
	{
		public:
			static int turnoJugador = 0;
			static Jugador ^jugadorEnTurno;

			Button ^hacerJugarAOponenteBoton;
			Button ^jugarBoton;
			Button ^sacarDeBarajaBoton;
			Button ^escogerAmarilloBoton;
			Button ^escogerAzulBoton;
			Button ^escogerRojoBoton;
			Button ^escogerVerdeBoton;

			static Carta ^cartaEnMeza;
			static Colores nuevoColorComodin = Colores::ninguno;

			Carta ^cartaEscogida;
			Juego();

			static int obtenerSiguienteTurno(int turno);
			static int obtenerSiguienteTurno();
			static void cambiarTurnoATurno(int nuevoTurno);
			static void cambiarTurno();

			void hacerJugarAOponente(Object ^sender, MouseEventArgs ^e);
			void jugar(Object ^sender, MouseEventArgs ^e);
			void sacarDeBaraja(Object ^sender, MouseEventArgs ^e);
			void escogerAmarillo(Object ^sender, MouseEventArgs ^e);
			void escogerAzul(Object ^sender, MouseEventArgs ^e);
			void escogerRojo(Object ^sender, MouseEventArgs ^e);
			void escogerVerde(Object ^sender, MouseEventArgs ^e);

			Carta^ obtenerCartaPorCoordenadas(int x, int y);

			// Metodos de la Escena
			void antesDeSerActivada() override;
			void antesDeSerDesactivada() override;
			void timerTick(Object ^sender, EventArgs ^e);
			void mouseClick(Object ^sender, MouseEventArgs ^e);

			void jugarCarta(Carta ^carta, bool resetearColorComodin);
			void jugarCarta(Carta ^carta);
			void jugarAlgunaDeEstasCartas(List<Carta ^> ^cartas);
			void sacarCartaYJugar();
			bool noSePuedeJugarConCartasActuales();
			
			void mostrarInfoJuego(Graphics ^graphics);
			void mostrarMisCartas(Graphics ^graphics);
			void mostarCartaEnMesa(Graphics ^graphics);
			void mostrarColorComodin(Graphics ^graphics);
			void mostrarCartaEscogida(Graphics ^graphics);

			void static validarGanador();
	};
}