#pragma once
#include "Escena.h"

namespace TrabajoFinal
{
	public ref class SeleccionJugadores : public Escena
	{
	public:
		SeleccionJugadores();
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
		void mostrarGraficos(Graphics^ graphics);
	};
}