#include "SeleccionJugadores.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	void crearJugadores(int numeroJugadores)
	{
		WinForm::NUMERO_JUGADORES = numeroJugadores;

		for (int i = 0; i < WinForm::NUMERO_JUGADORES; i++)
		{
			WinForm::jugadores->Add(gcnew Jugador());
		}

		WinForm::yo = WinForm::jugadores[0];
	}

	SeleccionJugadores::SeleccionJugadores()
	{
		onTimerTick = gcnew EventHandler(this, &SeleccionJugadores::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &SeleccionJugadores::teclaDown);
	}
	void SeleccionJugadores::timerTick(System::Object ^ sender, System::EventArgs ^ e)
	{
		if (activo)
		{
			buffer->Graphics->DrawImage(Imagenes::FondoDeDados, Rectangle(0, 0, WinForm::ANCHO, WinForm::ALTURA));
			mostrarGraficos(buffer->Graphics);
			buffer->Render(WinForm::graphics);
			dibujado = true;
		}
	}
	void SeleccionJugadores::teclaDown(System::Object ^ sender, System::Windows::Forms::KeyEventArgs ^ e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::D2)
				crearJugadores(2);

			else if (e->KeyCode == Keys::D3)
				crearJugadores(3);

			else if (e->KeyCode == Keys::D4)
				crearJugadores(4);

			else if (e->KeyCode == Keys::D5)
				crearJugadores(5);
			else
				return;

			DesactivarEscena(this);
			WinForm::reparticionCartas = gcnew ReparticionCartas();
			ActivarEscena(WinForm::reparticionCartas);
		}
	}

	void SeleccionJugadores::mostrarGraficos(Graphics^ graphics)
	{
		graphics->DrawString("INGRESA EL NUMERO DE JUGADORES", Fuentes::GRANDE_GRUESA, gcnew SolidBrush(Color::White), 280.0f, 40.0f);
		graphics->DrawString("2 - 5", Fuentes::GRANDE_GRUESA, gcnew SolidBrush(Color::White), 600.0f, 500.0f);
	}

}