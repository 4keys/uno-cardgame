#include "Carta.h"
#include "CartaCambioSentido.h"
#include "CartaComodin.h"
#include "CartaPierdeTurno.h"
#include "CartaRobaCuatro.h"
#include "CartaRobaDos.h"

namespace TrabajoFinal
{
	Carta::Carta()
	{
		;
	}
	Carta ^Carta::crearCarta(Colores color, Valores valor)
	{
		Carta ^carta;

		switch (valor) {
		case mas2: carta = gcnew CartaRobaDos(); break;
		case reversa: carta = gcnew CartaCambioSentido(); break;
		case bloqueo: carta = gcnew CartaPierdeTurno(); break;
		case mas4: carta = gcnew CartaRobaCuatro(); break;
		case cambioColor: carta = gcnew CartaComodin(); break;
		default: carta = gcnew Carta();
		}

		carta->color = color;
		carta->valor = valor;
		carta->puntaje = valor + 1;

		// Puntajes
		if (carta->valor == mas2 || carta->valor == reversa || carta->valor == bloqueo) {
			carta->puntaje = 20;
		}
		else if (carta->color == especial) {
			carta->puntaje = 50;
		}

		// Imagenes
		if (carta->color == rojo) {
			if (carta->valor == cero) carta->imagen = Imagenes::R0;
			if (carta->valor == uno) carta->imagen = Imagenes::R1;
			if (carta->valor == dos) carta->imagen = Imagenes::R2;
			if (carta->valor == tres) carta->imagen = Imagenes::R3;
			if (carta->valor == cuatro) carta->imagen = Imagenes::R4;
			if (carta->valor == cinco) carta->imagen = Imagenes::R5;
			if (carta->valor == seis) carta->imagen = Imagenes::R6;
			if (carta->valor == siete) carta->imagen = Imagenes::R7;
			if (carta->valor == ocho) carta->imagen = Imagenes::R8;
			if (carta->valor == nueve) carta->imagen = Imagenes::R9;
			if (carta->valor == reversa) carta->imagen = Imagenes::RR;
			if (carta->valor == mas2) carta->imagen = Imagenes::Rm2;
			if (carta->valor == bloqueo) carta->imagen = Imagenes::RB;
		}
		else if (carta->color == azul) {
			if (carta->valor == cero) carta->imagen = Imagenes::B0;
			if (carta->valor == uno) carta->imagen = Imagenes::B1;
			if (carta->valor == dos) carta->imagen = Imagenes::B2;
			if (carta->valor == tres) carta->imagen = Imagenes::B3;
			if (carta->valor == cuatro) carta->imagen = Imagenes::B4;
			if (carta->valor == cinco) carta->imagen = Imagenes::B5;
			if (carta->valor == seis) carta->imagen = Imagenes::B6;
			if (carta->valor == siete) carta->imagen = Imagenes::B7;
			if (carta->valor == ocho) carta->imagen = Imagenes::B8;
			if (carta->valor == nueve) carta->imagen = Imagenes::B9;
			if (carta->valor == reversa) carta->imagen = Imagenes::BR;
			if (carta->valor == mas2) carta->imagen = Imagenes::Bm2;
			if (carta->valor == bloqueo) carta->imagen = Imagenes::BB;
		}
		else if (carta->color == amarillo) {
			if (carta->valor == cero) carta->imagen = Imagenes::Y0;
			if (carta->valor == uno) carta->imagen = Imagenes::Y1;
			if (carta->valor == dos) carta->imagen = Imagenes::Y2;
			if (carta->valor == tres) carta->imagen = Imagenes::Y3;
			if (carta->valor == cuatro) carta->imagen = Imagenes::Y4;
			if (carta->valor == cinco) carta->imagen = Imagenes::Y5;
			if (carta->valor == seis) carta->imagen = Imagenes::Y6;
			if (carta->valor == siete) carta->imagen = Imagenes::Y7;
			if (carta->valor == ocho) carta->imagen = Imagenes::Y8;
			if (carta->valor == nueve) carta->imagen = Imagenes::Y9;
			if (carta->valor == reversa) carta->imagen = Imagenes::YR;
			if (carta->valor == mas2) carta->imagen = Imagenes::Ym2;
			if (carta->valor == bloqueo) carta->imagen = Imagenes::YB;
		}
		else if (carta->color == verde) {
			if (carta->valor == cero) carta->imagen = Imagenes::G0;
			if (carta->valor == uno) carta->imagen = Imagenes::G1;
			if (carta->valor == dos) carta->imagen = Imagenes::G2;
			if (carta->valor == tres) carta->imagen = Imagenes::G3;
			if (carta->valor == cuatro) carta->imagen = Imagenes::G4;
			if (carta->valor == cinco) carta->imagen = Imagenes::G5;
			if (carta->valor == seis) carta->imagen = Imagenes::G6;
			if (carta->valor == siete) carta->imagen = Imagenes::G7;
			if (carta->valor == ocho) carta->imagen = Imagenes::G8;
			if (carta->valor == nueve) carta->imagen = Imagenes::G9;
			if (carta->valor == reversa) carta->imagen = Imagenes::GR;
			if (carta->valor == mas2) carta->imagen = Imagenes::Gm2;
			if (carta->valor == bloqueo) carta->imagen = Imagenes::GB;
		}
		else if (carta->color == especial) {
			if (carta->valor == mas4) carta->imagen = Imagenes::m4;
			if (carta->valor == cambioColor) carta->imagen = Imagenes::cC;
		}

		return carta;
	}

	int Carta::obtenerPuntacion(List<Carta ^> ^cartas)
	{
		int puntaje = 0;
		for (int i = 0; i < cartas->Count; i++) {
			puntaje += cartas[i]->puntaje;
		}
		return puntaje;
	}
}