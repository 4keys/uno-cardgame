#pragma once
#include "Fuentes.h"
#include "Posicion.h"
#include "OrdenTurnos.h"

#include "SeleccionJugadores.h"
#include "ReparticionCartas.h"
#include "Inicio.h"
#include "Juego.h"
#include "Puntuacion.h"

namespace TrabajoFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Juego
	/// </summary>
	public ref class WinForm : public System::Windows::Forms::Form
	{
	public:
		static WinForm ^winform;
		static int ANCHO = 1280;
		static int ALTURA = 720;
		static Random ^aleatorio;

		// Probabilidad que un jugador quiera sacar una carta del mazo, voluntariamente
		static float PROBABILIDAD_JUGADOR_QUIERA_SACAR_CARTA = 10; //%
		static int PUNTAJE_PARA_GANAR_TODO = 500;
		static int NUMERO_DE_CARTAS_POR_JUGADOR_INICIAL = 7;

		static Imagenes ^imagenes;
		static Fuentes ^fuentes;

		static int NUMERO_JUGADORES;
		static OrdenTurnos ordenTurnos = OrdenTurnos::ASCENDENTE;
		static List<Jugador ^> ^jugadores;
		static Jugador ^yo;
		static List<Carta ^> ^mazo;

		static Jugador ^ganadorDeLaPartida;
		static bool ganadorEsGanadorAbsoluto = false;
		
		static Graphics ^graphics;
		static BufferedGraphicsContext ^context;

		static Inicio ^inicioEscena;
		static SeleccionJugadores ^seleccionJugadores;
		static ReparticionCartas ^reparticionCartas;
		static Juego ^juegoEscena;
		static Puntuacion ^puntuacionEscena;

		System::Windows::Forms::Timer^ timer;
		
		WinForm(void);
		~WinForm();

		static void darCartaAJugador(Carta^ carta, Jugador^ jugador);
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::IContainer ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->timer = (gcnew System::Windows::Forms::Timer(this->components));
			this->SuspendLayout();
			// 
			// timer
			// 
			this->timer->Enabled = true;
			this->timer->Interval = 50;
			// 
			// Winform
			// 
			this->KeyPreview = true;
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(WinForm::ANCHO, WinForm::ALTURA);
			this->Margin = System::Windows::Forms::Padding(0);
			this->MaximizeBox = false;
			this->Name = L"UNO";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"UNO";
			this->ResumeLayout(false);
		}
#pragma endregion
	};
}
