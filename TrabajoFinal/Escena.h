#pragma once
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace TrabajoFinal
{
	public ref class Escena
	{
		public:
			int contador;
			bool activo;
			bool dibujado;
			BufferedGraphics^ buffer;
			KeyEventHandler^ onKeyDown;
			KeyEventHandler^ onKeyUp;
			MouseEventHandler^ onMouseClick;
			EventHandler^ onTimerTick;
			Escena();
			static void ActivarEscena(Escena^ escena);
			static void DesactivarEscena(Escena^ escena);

			virtual void antesDeSerActivada();
			virtual void antesDeSerDesactivada();
	};
}
