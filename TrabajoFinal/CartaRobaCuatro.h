#pragma once
#include "CartaEspecial.h"

namespace TrabajoFinal
{
	public ref class CartaRobaCuatro : public CartaEspecial
	{
	public:
		CartaRobaCuatro();
		void aplicarEfecto() override;
	};
}