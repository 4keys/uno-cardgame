#include "Fuentes.h"

namespace TrabajoFinal
{
	Fuentes::Fuentes()
	{
		Fuentes::GRANDE = gcnew Font("ARIAL", 24.0f, FontStyle::Regular, GraphicsUnit::Point);
		Fuentes::GRANDE_GRUESA = gcnew Font("ARIAL", 24.0f, FontStyle::Bold, GraphicsUnit::Point);
		Fuentes::MEDIANA = gcnew Font("ARIAL", 18.0f, FontStyle::Regular, GraphicsUnit::Point);
		Fuentes::MEDIANA_GRUESA = gcnew Font("ARIAL", 18.0f, FontStyle::Bold, GraphicsUnit::Point);
		Fuentes::PEQUENIA = gcnew Font("ARIAL", 15.0f, FontStyle::Regular, GraphicsUnit::Point);
	}
}
