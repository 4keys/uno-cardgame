#include "Puntuacion.h"
#include "WinForm.h"
#include <cmath>
#include <algorithm>

namespace TrabajoFinal
{
	Puntuacion::Puntuacion()
	{
		onTimerTick = gcnew EventHandler(this, &Puntuacion::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &Puntuacion::teclaDown);
	}
	void Puntuacion::timerTick(System::Object ^ sender, System::EventArgs ^ e)
	{
		if (activo)
		{
			buffer->Graphics->DrawImage(Imagenes::Puntuacion, Rectangle(0, 0, WinForm::ANCHO, WinForm::ALTURA));
			mostrarInfoJuego(buffer->Graphics);
			buffer->Render(WinForm::graphics);
			dibujado = true;
		}
	}
	void Puntuacion::teclaDown(Object ^sender, KeyEventArgs ^e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::Enter) {
				DesactivarEscena(this);
				if (WinForm::ganadorEsGanadorAbsoluto) {
					WinForm::winform->Close();
					return;
				}
				WinForm::reparticionCartas = gcnew ReparticionCartas();
				ActivarEscena(WinForm::reparticionCartas);
			}
		}
	}

	void Puntuacion::mostrarInfoJuego(Graphics^ graphics)
	{
		String^ ganadorLabel = WinForm::ganadorEsGanadorAbsoluto ? "GANO TODO" : "Ganador";
		int indiceGanador = WinForm::jugadores->IndexOf(WinForm::ganadorDeLaPartida);
		graphics->DrawString(ganadorLabel + "", Fuentes::GRANDE_GRUESA, gcnew SolidBrush(Color::White), 39.0f + indiceGanador * 177, 169.0f);

		for (int i = 0; i < WinForm::jugadores->Count; i++) {
			Jugador ^jugador = WinForm::jugadores[i];
			int puntaje = jugador->puntaje;
			graphics->DrawString(puntaje + " puntos", Fuentes::PEQUENIA, gcnew SolidBrush(Color::White), 50.0f + i * 255, 265.0f);

			int fichas20s = (int)ceil(puntaje / 20);
			int restante20s = puntaje - (20 * fichas20s);
			int fichas10s = (int)ceil(restante20s / 10);
			int restante10s = puntaje - (10 * fichas10s);
			int fichas5s = (int)ceil(restante10s / 5);
			int restante5s = puntaje - (5 * fichas5s);
			int fichas1s = restante5s;

			int espacioEntreMonedas = 10;
			int maximoDeMonedas = 30;
			int coordenadaYInicio = 640;

			for (int j = 0; j < std::min(fichas20s, maximoDeMonedas); j++) {
				graphics->FillEllipse(gcnew SolidBrush(Color::Red), Rectangle(256 * i, coordenadaYInicio - (j * espacioEntreMonedas), 64, 64));
				graphics->DrawEllipse(gcnew Pen(Color::Gray, 6), Rectangle(256 * i, coordenadaYInicio - (j * espacioEntreMonedas), 64, 64));
			}
			if (fichas20s > 0) {
				graphics->DrawString("20", Fuentes::PEQUENIA, gcnew SolidBrush(Color::Black), 256 * i + 18, coordenadaYInicio - (std::min(fichas20s, maximoDeMonedas) * espacioEntreMonedas) - 15);
			}
			for (int j = 0; j < std::min(fichas10s, maximoDeMonedas); j++) {
				graphics->FillEllipse(gcnew SolidBrush(Color::DeepSkyBlue), Rectangle(256 * i + 64, coordenadaYInicio - (j * espacioEntreMonedas), 64, 64));
				graphics->DrawEllipse(gcnew Pen(Color::Gray, 6), Rectangle(256 * i + 64, coordenadaYInicio - (j * espacioEntreMonedas), 64, 64));
			}
			if (fichas10s > 0) {
				graphics->DrawString("10", Fuentes::PEQUENIA, gcnew SolidBrush(Color::Black), 256 * i + 88, coordenadaYInicio - (std::min(fichas10s, maximoDeMonedas) * espacioEntreMonedas) - 15);
			}
			for (int j = 0; j < std::min(fichas5s, maximoDeMonedas); j++) {
				graphics->FillEllipse(gcnew SolidBrush(Color::Yellow), Rectangle(256 * i + 128, coordenadaYInicio - (j * espacioEntreMonedas), 64, 64));
				graphics->DrawEllipse(gcnew Pen(Color::Gray, 6), Rectangle(256 * i + 128, coordenadaYInicio - (j * espacioEntreMonedas), 64, 64));
			}
			if (fichas5s > 0) {
				graphics->DrawString("5", Fuentes::PEQUENIA, gcnew SolidBrush(Color::Black), 256 * i + 146, coordenadaYInicio - (std::min(fichas5s, maximoDeMonedas) * espacioEntreMonedas) - 15);
			}
			for (int j = 0; j < std::min(fichas1s, maximoDeMonedas); j++) {
				graphics->FillEllipse(gcnew SolidBrush(Color::GreenYellow), Rectangle(256 * i + 192, coordenadaYInicio - (j * espacioEntreMonedas), 64, 64));
				graphics->DrawEllipse(gcnew Pen(Color::Gray, 6), Rectangle(256 * i + 192, coordenadaYInicio - (j * espacioEntreMonedas), 64, 64));
			}
			if (fichas1s > 0) {
				graphics->DrawString("1", Fuentes::PEQUENIA, gcnew SolidBrush(Color::Black), 256 * i + 210, coordenadaYInicio - (std::min(fichas1s, maximoDeMonedas) * espacioEntreMonedas) - 15);
			}
		}
	}

}