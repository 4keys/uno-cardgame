#pragma once
#include "CartaEspecial.h"

namespace TrabajoFinal
{
	public ref class CartaCambioSentido : public CartaEspecial
	{
	public:
		CartaCambioSentido();
		void aplicarEfecto() override;
	};
}