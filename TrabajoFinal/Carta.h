#pragma once
#include "Imagenes.h"
#include "Colores.h"
#include "Valores.h"

using namespace System::Collections::Generic;

namespace TrabajoFinal
{
	public ref class Carta
	{
		public:
			Bitmap ^imagen;
			Colores color;
			Valores valor;
			int puntaje;
			Carta();

		static Carta ^crearCarta(Colores color, Valores valor);
		static int obtenerPuntacion(List<Carta ^> ^cartas);
	};
}