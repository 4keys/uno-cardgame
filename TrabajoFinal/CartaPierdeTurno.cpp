#pragma once
#include "CartaPierdeTurno.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	CartaPierdeTurno::CartaPierdeTurno() : CartaEspecial() { ; }

	void CartaPierdeTurno::aplicarEfecto() {
		int siguienteTurno = Juego::obtenerSiguienteTurno();
		int siguienteTurnoDelSiguienteTurno = Juego::obtenerSiguienteTurno(siguienteTurno);
		Juego::cambiarTurnoATurno(siguienteTurnoDelSiguienteTurno);
	};
}