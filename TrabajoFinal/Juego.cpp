#include "Juego.h"
#include "CartaEspecial.h"
#include "WinForm.h"
#include <cmath>

using namespace System;
using namespace System::Media;

namespace TrabajoFinal
{
	Juego::Juego()
	{
		onTimerTick = gcnew EventHandler(this, &Juego::timerTick);
		onMouseClick = gcnew MouseEventHandler(this, &Juego::mouseClick);

		hacerJugarAOponenteBoton = gcnew Button();
		hacerJugarAOponenteBoton->AutoSize = true;
		hacerJugarAOponenteBoton->TextAlign = ContentAlignment::MiddleCenter;
		hacerJugarAOponenteBoton->SetBounds(865, 40, 50, 50);
		hacerJugarAOponenteBoton->Text = "Jugador ##: Jugar";

		jugarBoton = gcnew Button();
		jugarBoton->AutoSize = true;
		jugarBoton->TextAlign = ContentAlignment::MiddleCenter;
		jugarBoton->SetBounds(865, 140, 50, 50);
		jugarBoton->Text = "Jugar";

		sacarDeBarajaBoton = gcnew Button();
		sacarDeBarajaBoton->AutoSize = true;
		sacarDeBarajaBoton->TextAlign = ContentAlignment::MiddleCenter;
		sacarDeBarajaBoton->SetBounds(910, 140, 50, 50);
		sacarDeBarajaBoton->Text = "Sacar de baraja";

		escogerAmarilloBoton = gcnew Button();
		escogerAmarilloBoton->TextAlign = ContentAlignment::MiddleCenter;
		escogerAmarilloBoton->SetBounds(530, 223, 70, 50);
		escogerAmarilloBoton->Text = "Amarillo";
		escogerAmarilloBoton->Hide();
		escogerAzulBoton = gcnew Button();
		escogerAzulBoton->TextAlign = ContentAlignment::MiddleCenter;
		escogerAzulBoton->SetBounds(600, 223, 50, 50);
		escogerAzulBoton->Text = "Azul";
		escogerAzulBoton->Hide();
		escogerRojoBoton = gcnew Button();
		escogerRojoBoton->TextAlign = ContentAlignment::MiddleCenter;
		escogerRojoBoton->SetBounds(650, 223, 50, 50);
		escogerRojoBoton->Text = "Rojo";
		escogerRojoBoton->Hide();
		escogerVerdeBoton = gcnew Button();
		escogerVerdeBoton->TextAlign = ContentAlignment::MiddleCenter;
		escogerVerdeBoton->SetBounds(700, 223, 50, 50);
		escogerVerdeBoton->Text = "Verde";
		escogerVerdeBoton->Hide();
	}

	void Juego::antesDeSerActivada()
	{
		// Mostramos todos los botones y controles de esta escena
		WinForm::winform->Controls->Add(hacerJugarAOponenteBoton);
		WinForm::winform->Controls->Add(jugarBoton);
		WinForm::winform->Controls->Add(sacarDeBarajaBoton);
		hacerJugarAOponenteBoton->MouseClick += gcnew MouseEventHandler(this, &Juego::hacerJugarAOponente);
		jugarBoton->MouseClick += gcnew MouseEventHandler(this, &Juego::jugar);;
		sacarDeBarajaBoton->MouseClick += gcnew MouseEventHandler(this, &Juego::sacarDeBaraja);;

		WinForm::winform->Controls->Add(escogerAmarilloBoton);
		escogerAmarilloBoton->MouseClick += gcnew MouseEventHandler(this, &Juego::escogerAmarillo);
		WinForm::winform->Controls->Add(escogerAzulBoton);
		escogerAzulBoton->MouseClick += gcnew MouseEventHandler(this, &Juego::escogerAzul);
		WinForm::winform->Controls->Add(escogerRojoBoton);
		escogerRojoBoton->MouseClick += gcnew MouseEventHandler(this, &Juego::escogerRojo);
		WinForm::winform->Controls->Add(escogerVerdeBoton);
		escogerVerdeBoton->MouseClick += gcnew MouseEventHandler(this, &Juego::escogerVerde);
	}

	void Juego::antesDeSerDesactivada()
	{
		// Ocultamos todos los botones y controles de esta escena
		WinForm::winform->Controls->Remove(hacerJugarAOponenteBoton);
		WinForm::winform->Controls->Remove(jugarBoton);
		WinForm::winform->Controls->Remove(sacarDeBarajaBoton);
		hacerJugarAOponenteBoton->MouseClick -= gcnew MouseEventHandler(this, &Juego::hacerJugarAOponente);
		jugarBoton->MouseClick -= gcnew MouseEventHandler(this, &Juego::jugar);
		jugarBoton->MouseClick -= gcnew MouseEventHandler(this, &Juego::sacarDeBaraja);

		WinForm::winform->Controls->Remove(escogerAmarilloBoton);
		escogerAmarilloBoton->MouseClick -= gcnew MouseEventHandler(this, &Juego::escogerAmarillo);
		WinForm::winform->Controls->Remove(escogerAzulBoton);
		escogerAzulBoton->MouseClick -= gcnew MouseEventHandler(this, &Juego::escogerAzul);
		WinForm::winform->Controls->Remove(escogerRojoBoton);
		escogerRojoBoton->MouseClick -= gcnew MouseEventHandler(this, &Juego::escogerRojo);
		WinForm::winform->Controls->Remove(escogerVerdeBoton);
		escogerVerdeBoton->MouseClick -= gcnew MouseEventHandler(this, &Juego::escogerVerde);
	}

	bool mismoColorQueCartaDeMesa(Carta ^carta)
	{
		Colores colorMeza = Juego::cartaEnMeza->color;
		if (Juego::nuevoColorComodin != Colores::ninguno) colorMeza = Juego::nuevoColorComodin;

		return carta->color == colorMeza;
	}

	bool mismoNumeroQueCartaDeMesa(Carta ^carta)
	{
		return carta->valor <= 9 && (carta->valor == Juego::cartaEnMeza->valor);
	}

	bool esEspecial(Carta ^carta)
	{
		return carta->valor >= 10;
	}

	bool esComodin(Carta ^carta)
	{
		return carta->valor == Valores::mas4 || carta->valor == Valores::cambioColor;
	}

	bool esCartaJugable(Carta ^carta)
	{
		return esComodin(carta) || mismoNumeroQueCartaDeMesa(carta) || mismoColorQueCartaDeMesa(carta);
	}

	void Juego::hacerJugarAOponente(Object^ sender, MouseEventArgs^ e)
	{
		// Lo Primero que podr� hacer un jugador es decidir si va a sacar una carta de la baraja o no.
		
		// ========================================
		// 0: JUGADA "PROBANDO SUERTE DE LA BARAJA"
		// ========================================
		int probabilidadRandom = WinForm::aleatorio->Next(100);
		if (probabilidadRandom <= WinForm::PROBABILIDAD_JUGADOR_QUIERA_SACAR_CARTA) {
			sacarCartaYJugar();
			return;
		}

		// Si decide no sacar una carta de la baraja, lo cual es lo normal, podr� hacer 4 jugadas:

		// =========================
		// 1: JUGADA POR MISMO COLOR
		// =========================

		// ==========================
		// 2: JUGADA POR MISMO NUMERO
		// ==========================

		// Se decidir� de forma aleatoria c�al de estas dos jugadas se emplear�.
		// Escogeremos un Plan A o un Plan B
		int uno_o_dos = WinForm::aleatorio->Next(2) + 1;
		List<Carta^> ^cartasPlanA, ^cartasPlanB;
		
		if (uno_o_dos == 1) {
			// Si sale "1", se INTENTAR� jugar con el mismo color, y luego con el mismo n�mero
			cartasPlanA = Juego::jugadorEnTurno->mano->FindAll(gcnew Predicate<Carta^>(mismoColorQueCartaDeMesa));
			cartasPlanB = Juego::jugadorEnTurno->mano->FindAll(gcnew Predicate<Carta^>(mismoNumeroQueCartaDeMesa));
		}
		else if (uno_o_dos == 2) {
			// Si sale "2", se INTENTAR� jugar con el mismo n�mero, y luego con el mismo color
			cartasPlanA = Juego::jugadorEnTurno->mano->FindAll(gcnew Predicate<Carta^>(mismoNumeroQueCartaDeMesa));
			cartasPlanB = Juego::jugadorEnTurno->mano->FindAll(gcnew Predicate<Carta^>(mismoColorQueCartaDeMesa));
		}

		// Verificamos si nuestro plan A, es posible
		if (cartasPlanA->Count > 0) {
			jugarAlgunaDeEstasCartas(cartasPlanA);
			return;
		}
		// Si no es posible, verificamos si nuestro plan B, es posible
		if (cartasPlanB->Count > 0) {
			jugarAlgunaDeEstasCartas(cartasPlanB);
			return;
		}

		// Si no es posible jugar ni el plan A ni el plan B
		// Se intentara jugar:

		// ===============================
		// 3: JUGADA CON CARTAS ESPECIALES
		// ===============================

		List<Carta ^> ^cartasEspeciales = Juego::jugadorEnTurno->mano->FindAll(gcnew Predicate<Carta^>(esComodin));
		// Verificamos si tenemos cartas especiales
		if (cartasEspeciales->Count > 0) {
			jugarAlgunaDeEstasCartas(cartasEspeciales);
			return;
		}
		// ========================================================
		// 4: JUGADA "SACANDO DE LA BARAJA PORQUE NO QUEDA DE OTRA"
		// ========================================================

		sacarCartaYJugar();
		return;
	}

	/* Evento que se da cuando se le da click al bot�n "Jugar" */
	void Juego::jugar(Object ^sender, MouseEventArgs ^e)
	{
		if (cartaEscogida != nullptr) {
			if (esComodin(cartaEscogida) && Juego::nuevoColorComodin == Colores::ninguno) {
				MessageBox::Show("Escoge un color!");
			}
			else {
				jugarCarta(cartaEscogida, false);
				cartaEscogida = nullptr;
				escogerAmarilloBoton->Hide();
				escogerAzulBoton->Hide();
				escogerRojoBoton->Hide();
				escogerVerdeBoton->Hide();
			}
		}
		else {
			MessageBox::Show("Primero escoge la carta que quieras jugar!");
		}
	}

	void Juego::sacarDeBaraja(Object ^sender, MouseEventArgs ^e)
	{
		sacarCartaYJugar();
		return;
	}

	void Juego::escogerAmarillo(Object ^sender, MouseEventArgs ^e)
	{
		Juego::nuevoColorComodin = Colores::amarillo;
	}

	void Juego::escogerAzul(Object ^sender, MouseEventArgs ^e)
	{
		Juego::nuevoColorComodin = Colores::azul;
	}

	void Juego::escogerRojo(Object ^sender, MouseEventArgs ^e)
	{
		Juego::nuevoColorComodin = Colores::rojo;
	}

	void Juego::escogerVerde(Object ^sender, MouseEventArgs ^e)
	{
		Juego::nuevoColorComodin = Colores::verde;
	}

	bool Juego::noSePuedeJugarConCartasActuales() {
		List<Carta ^> ^cartasActuales = Juego::jugadorEnTurno->mano;
		Carta^ cartaJugable = cartasActuales->Find(gcnew Predicate<Carta^>(esCartaJugable));
		return cartaJugable == nullptr;
	}

	void Juego::timerTick(System::Object ^sender, System::EventArgs ^e)
	{
		if (activo) {
			buffer->Graphics->DrawImage(Imagenes::Campo, Rectangle(0, 0, WinForm::ANCHO, WinForm::ALTURA));
			mostrarInfoJuego(buffer->Graphics);
			mostrarMisCartas(buffer->Graphics);
			mostarCartaEnMesa(buffer->Graphics);
			
			if (cartaEscogida != nullptr) {
				mostrarCartaEscogida(buffer->Graphics);
			}

			if (Juego::nuevoColorComodin != Colores::ninguno) {
				mostrarColorComodin(buffer->Graphics);
			}

			buffer->Render(WinForm::graphics);
			dibujado = true;
		}
	}

	void Juego::mouseClick(System::Object ^sender, MouseEventArgs ^e)
	{
		if (activo && dibujado && Juego::jugadorEnTurno == WinForm::yo) {
			Carta ^cartaClickeada = obtenerCartaPorCoordenadas(e->X, e->Y);
			if (cartaClickeada == nullptr) {
				cartaEscogida = nullptr;
			}
			else if (esCartaJugable(cartaClickeada)) {
				cartaEscogida = cartaClickeada;
			}

			if (cartaEscogida != nullptr && esComodin(cartaEscogida)) {
				escogerAmarilloBoton->Show();
				escogerAzulBoton->Show();
				escogerRojoBoton->Show();
				escogerVerdeBoton->Show();
			}
			else {
				escogerAmarilloBoton->Hide();
				escogerAzulBoton->Hide();
				escogerRojoBoton->Hide();
				escogerVerdeBoton->Hide();
			}
		}
	}

	void Juego::jugarCarta(Carta ^carta, bool resetearColorComodin)
	{
		if (resetearColorComodin) {
			Juego::nuevoColorComodin = Colores::ninguno;
		}

		Juego::jugadorEnTurno->mano->Remove(carta);
		cartaEnMeza = carta;
		if (esEspecial(carta)) {
			dynamic_cast<CartaEspecial ^>(carta)->aplicarEfecto();
		}
		else {
			cambiarTurno();
		}
	}

	void Juego::jugarCarta(Carta ^carta) { jugarCarta(carta, true); }

	void Juego::jugarAlgunaDeEstasCartas(List<Carta ^> ^cartas)
	{
		int indiceRandom = WinForm::aleatorio->Next(cartas->Count);
		jugarCarta(cartas[indiceRandom]);
	}

	void Juego::sacarCartaYJugar()
	{
		Carta ^ultimaCarta = WinForm::mazo[0];
		WinForm::mazo->Remove(ultimaCarta);

		if (Juego::jugadorEnTurno != WinForm::yo) {
			MessageBox::Show("Jugador " + (Juego::turnoJugador + 1) + " sac� carta de baraja!");
		}

		Juego::jugadorEnTurno->mano->Add(ultimaCarta);

		if (esCartaJugable(ultimaCarta)) {
			jugarCarta(ultimaCarta, false);
		}
		else {
			Juego::cambiarTurno();
		}

	}

	Carta ^Juego::obtenerCartaPorCoordenadas(int x, int y)
	{
		int indice = 0;

		if (y >= 337 && y <= 449) {
			float posibleIndice = (x - 37.0f) / 137;
			if (posibleIndice + (25.0f / 137) > ceil(posibleIndice)) {
				return nullptr;
			}
			indice = (int) floor(posibleIndice);
		}
		else if (y >= 526 && y <= 638) {
			float posibleIndice = (x - 37.0f) / 137;
			if (posibleIndice + (25.0f / 137) > ceil(posibleIndice)) {
				return nullptr;
			}
			indice = (int)floor(posibleIndice) + 9;
		}
		else {
			return nullptr;
		}

		if (indice >= 0 && indice < WinForm::yo->mano->Count) {
			return WinForm::yo->mano[indice];
		}
		return nullptr;
	}

	void Juego::mostrarInfoJuego(Graphics^ graphics)
	{
		int ESPACIO_BLANCO = 21;
		graphics->DrawString("Turno: Jugador " + (Juego::turnoJugador + 1), Fuentes::MEDIANA, gcnew SolidBrush(Color::White), 6, 6);
		graphics->DrawString("Numero de Cartas", Fuentes::MEDIANA_GRUESA, gcnew SolidBrush(Color::White), 6, 30);
		for (int i = 0; i < WinForm::NUMERO_JUGADORES; i++) {
			graphics->DrawString("Jugador "+ ( i + 1) +": " + WinForm::jugadores[i]->mano->Count, Fuentes::PEQUENIA, gcnew SolidBrush(Color::White), 6.0f, 62.0f + i * ESPACIO_BLANCO);
		}
	}

	void Juego::mostrarColorComodin(Graphics^ graphics)
	{
		String ^nuevoColorString;
		Color color;

		switch (Juego::nuevoColorComodin) {
		case Colores::amarillo: nuevoColorString = "Amarillo"; color = Color::Yellow; break;
		case Colores::azul: nuevoColorString = "Azul"; color = Color::DeepSkyBlue;  break;
		case Colores::rojo: nuevoColorString = "Rojo"; color = Color::Red; break;
		case Colores::verde: nuevoColorString = "Verde"; color = Color::GreenYellow; break;
		}

		graphics->DrawString("Ahora Se Juega", Fuentes::MEDIANA_GRUESA, gcnew SolidBrush(color), 360, 63);
		graphics->DrawString("" + nuevoColorString, Fuentes::MEDIANA_GRUESA, gcnew SolidBrush(color), 420, 93);
	}

	void Juego::mostrarMisCartas(Graphics^ graphics)
	{
		for (int i = 0; i < WinForm::yo->mano->Count; i++) {
			int coordenadaX = 37 + (i * 137) - ((i < 9) ? 0 : 1233);
			int coordenadaY = (i < 9) ? 337 : 526;
			graphics->DrawImage(WinForm::yo->mano[i]->imagen, Rectangle(coordenadaX, coordenadaY, 112, 157));
		}
	}

	void Juego::mostarCartaEnMesa(Graphics^ graphics)
	{
		graphics->DrawImage(cartaEnMeza->imagen, Rectangle(584, 37, 112, 157));

	}
	void Juego::mostrarCartaEscogida(Graphics^ graphics)
	{
		int indiceCarta = WinForm::yo->mano->IndexOf(cartaEscogida);
		int coordenadaX = 30 + (indiceCarta * 137) - ((indiceCarta < 9) ? 0 : 1233);
		int coordenadaY = (indiceCarta < 9) ? 330 : 519;
		graphics->DrawRectangle(gcnew Pen(Color::OrangeRed, 10), Rectangle(coordenadaX, coordenadaY, 125, 170));
	}

	int Juego::obtenerSiguienteTurno(int turno)
	{
		if (WinForm::ordenTurnos == OrdenTurnos::ASCENDENTE) {
			int siguienteTurno = turno + 1;
			return siguienteTurno == WinForm::NUMERO_JUGADORES ? 0 : siguienteTurno;
		}
		else { // DESCENDENTE
			int siguienteTurno = turno - 1;
			return siguienteTurno == -1 ? WinForm::NUMERO_JUGADORES - 1 : siguienteTurno;
		}
	}

	int Juego::obtenerSiguienteTurno()
	{
		return obtenerSiguienteTurno(Juego::turnoJugador);
	}

	void Juego::cambiarTurno()
	{
		int siguienteTurno = obtenerSiguienteTurno();
		Juego::cambiarTurnoATurno(siguienteTurno);
	}

	void Juego::cambiarTurnoATurno(int nuevoTurno)
	{
		// Antes de Cambiar De Turno Verificar si el ultimo jugador que jugo ha ganado!

		if (jugadorEnTurno != nullptr && jugadorEnTurno->mano->Count == 0) {
			Juego::validarGanador();
			DesactivarEscena(WinForm::juegoEscena);
			ActivarEscena(WinForm::puntuacionEscena);
			return;
		}

		Juego::turnoJugador = nuevoTurno;
		Juego::jugadorEnTurno = WinForm::jugadores[nuevoTurno];

		// Activar el boton de "jugar", solo cuando sea mi turno
		WinForm::juegoEscena->hacerJugarAOponenteBoton->Text = "Jugador " + (nuevoTurno + 1) + ": Jugar";
		WinForm::juegoEscena->jugarBoton->Hide();
		WinForm::juegoEscena->sacarDeBarajaBoton->Hide();
		WinForm::juegoEscena->hacerJugarAOponenteBoton->Show();

		if (nuevoTurno == 0) {
			WinForm::juegoEscena->jugarBoton->Show();
			WinForm::juegoEscena->sacarDeBarajaBoton->Show();
			WinForm::juegoEscena->hacerJugarAOponenteBoton->Hide();
		}
	}

	void Juego::validarGanador()
	{
		Jugador ^ganador = jugadorEnTurno;
		WinForm::ganadorDeLaPartida = ganador;

		for (int i = 0; i < WinForm::jugadores->Count; i++) {
			Jugador ^jugador = WinForm::jugadores[i];

			if (jugador != ganador) {
				ganador->puntaje += Carta::obtenerPuntacion(jugador->mano);
			}
		}

		if (ganador->puntaje >= WinForm::PUNTAJE_PARA_GANAR_TODO) {
			WinForm::ganadorEsGanadorAbsoluto = true;
		}
	}
}

