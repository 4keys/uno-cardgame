#include "Escena.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	Escena::Escena()
	{
		buffer = WinForm::context->Allocate(WinForm::graphics, WinForm::winform->ClientRectangle);
		activo = false;
		dibujado = false;
		contador = 0;
	}

	void Escena::antesDeSerActivada() {
		;
	}

	void Escena::antesDeSerDesactivada() {
		;
	}


	void Escena::ActivarEscena(Escena^ escena)
	{
		escena->antesDeSerActivada();
		escena->activo = true;

		if (escena->onTimerTick != nullptr)
			WinForm::winform->timer->Tick += escena->onTimerTick;
		if (escena->onKeyDown != nullptr)
			WinForm::winform->KeyDown += escena->onKeyDown;
		if (escena->onKeyUp != nullptr)
			WinForm::winform->KeyUp += escena->onKeyUp;
		if (escena->onMouseClick != nullptr)
			WinForm::winform->MouseClick += escena->onMouseClick;
	}

	void Escena::DesactivarEscena(Escena^ escena)
	{
		escena->antesDeSerDesactivada();
		escena->activo = false;

		if (escena->onTimerTick != nullptr)
			WinForm::winform->timer->Tick -= escena->onTimerTick;
		if (escena->onKeyDown != nullptr)
			WinForm::winform->KeyDown -= escena->onKeyDown;
		if (escena->onKeyUp != nullptr)
			WinForm::winform->KeyUp -= escena->onKeyUp;
		if (escena->onMouseClick != nullptr)
			WinForm::winform->MouseClick -= escena->onMouseClick;

		escena->dibujado = false;
	}
}