#include "ReparticionCartas.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	ReparticionCartas::ReparticionCartas()
	{
		WinForm::mazo = gcnew List<Carta^>();

		onTimerTick = gcnew EventHandler(this, &ReparticionCartas::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &ReparticionCartas::teclaDown);
		inicializarMazo();
		barajarMazo();
		repartirCartas();
		obtenerPrimerJugador();
	}
	void ReparticionCartas::timerTick(System::Object ^sender, System::EventArgs ^e)
	{
		if (activo) {
			buffer->Graphics->DrawImage(Imagenes::ReparticionCartas, Rectangle(0, 0, WinForm::ANCHO, WinForm::ALTURA));
			mostrarInfoJuego(buffer->Graphics);
			buffer->Render(WinForm::graphics);
			dibujado = true;
		}
	}
	void ReparticionCartas::teclaDown(Object ^sender, KeyEventArgs ^e)
	{
		if (activo && dibujado) {
			if (e->KeyCode == Keys::Enter) {
				DesactivarEscena(this);
				ActivarEscena(WinForm::juegoEscena);
			}
		}
	}
	void ReparticionCartas::inicializarMazo()
	{
		WinForm::mazo = gcnew List<Carta ^>();

		for (int i = 0; i < 5; i++) {
			Colores color = Colores(i);
			if (i == 4) {
				WinForm::mazo->Add(Carta::crearCarta(color, mas4));
				WinForm::mazo->Add(Carta::crearCarta(color, mas4));
				WinForm::mazo->Add(Carta::crearCarta(color, mas4));
				WinForm::mazo->Add(Carta::crearCarta(color, mas4));
				WinForm::mazo->Add(Carta::crearCarta(color, cambioColor));
				WinForm::mazo->Add(Carta::crearCarta(color, cambioColor));
				WinForm::mazo->Add(Carta::crearCarta(color, cambioColor));
				WinForm::mazo->Add(Carta::crearCarta(color, cambioColor));
			}
			else {
				for (int j = 0; j < 13; j++) {
					Valores carta = Valores(j);
					WinForm::mazo->Add(Carta::crearCarta(color, carta));
					if (j != 0) {
						WinForm::mazo->Add(Carta::crearCarta(color, carta));
					}
				}
			}
		}
		
	}
	void ReparticionCartas::barajarMazo()
	{
		int contador = WinForm::mazo->Count;
		while (contador > 1) {
			contador--;
			int r = WinForm::aleatorio->Next(contador);
			Carta^ aux = WinForm::mazo[r];
			WinForm::mazo[r] = WinForm::mazo[contador];
			WinForm::mazo[contador] = aux;
		}
	}
	void ReparticionCartas::obtenerPrimerJugador()
	{
		int NUMEROS_PARA_CARTAS = 10;
		int NUMERO_DE_COLORES_PARA_CARTAS = 4;

		int mayor = 0;

		for (int i = 0; i < WinForm::NUMERO_JUGADORES; i++) {
			Valores valorRandom = Valores(WinForm::aleatorio->Next(NUMEROS_PARA_CARTAS));
			Colores colorRandom = Colores(i % NUMERO_DE_COLORES_PARA_CARTAS);

			Carta ^carta = Carta::crearCarta(colorRandom, valorRandom);

			WinForm::jugadores[i]->cartaSorteo = carta;

			if (valorRandom > mayor) {
				mayor = valorRandom;
				indiceJugadorConCartaMayor = i;
			}
		}

		Juego::turnoJugador = indiceJugadorConCartaMayor;
		Juego::cambiarTurnoATurno(indiceJugadorConCartaMayor);
		// El jugador que reparte es el que tiene la carta mayor
		// Pero, el que juega primero es el siguiente, por lo tanto, se tendr� que cambiar de turno inmediatamente
		Juego::cambiarTurno();

		Juego::cambiarTurnoATurno(0);
	}

	void ReparticionCartas::repartirCartas() {
		for (int indiceJugador = 0; indiceJugador < WinForm::NUMERO_JUGADORES; indiceJugador++) {
			WinForm::jugadores[indiceJugador]->mano = gcnew List<Carta ^>();;
			
			for (int i = 0; i < WinForm::NUMERO_DE_CARTAS_POR_JUGADOR_INICIAL; i++) {
				WinForm::darCartaAJugador(WinForm::mazo[0], WinForm::jugadores[indiceJugador]);
			}
		}

		// En esta etapa de reparticiones, tambi�n se pondr� la primera carta en la meza
		Juego::cartaEnMeza = WinForm::mazo[0];
		WinForm::mazo->RemoveAt(0);
	}

	void ReparticionCartas::mostrarInfoJuego(Graphics ^graphics) {
		for (int i = 0; i < WinForm::NUMERO_JUGADORES; i++) {
			graphics->DrawImage(WinForm::jugadores[i]->cartaSorteo->imagen, Rectangle(82 + i*(156 + 84), 336, 156, 230));
		}
		graphics->DrawString("Repartidor", Fuentes::GRANDE_GRUESA, gcnew SolidBrush(Color::White), 82.0f + indiceJugadorConCartaMayor*(156 + 84), 298.0f);
	}
}