#pragma once
#include "Escena.h"

namespace TrabajoFinal
{
	public ref class Inicio : public Escena
	{
		public:
			Inicio();
			void timerTick(System::Object^  sender, System::EventArgs^  e);
			void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
	};
}