#include "Inicio.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	Inicio::Inicio()
	{
		onTimerTick = gcnew EventHandler(this, &Inicio::timerTick);
		onKeyDown = gcnew KeyEventHandler(this, &Inicio::teclaDown);
	}

	void Inicio::timerTick(System::Object^  sender, System::EventArgs^  e)
	{
		if (activo)
		{
			buffer->Graphics->DrawImage(Imagenes::Inicio, Rectangle(0, 0, WinForm::ANCHO, WinForm::ALTURA));
			buffer->Render(WinForm::graphics);
			dibujado = true;
		}
	}

	void Inicio::teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
	{
		if (activo && dibujado)
		{
			if (e->KeyCode == Keys::Enter)
			{
				DesactivarEscena(this);
				ActivarEscena(WinForm::seleccionJugadores);
			}
		}
	}
}