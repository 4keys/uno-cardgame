#pragma once

namespace TrabajoFinal
{
	public ref class Posicion
	{
		public:
			float x;
			float y;
			Posicion(float x, float y);
		};
}