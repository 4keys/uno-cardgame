#pragma once
#include "Escena.h"
#include "Fuentes.h"
#include "Imagenes.h"

namespace TrabajoFinal
{
	public ref class Puntuacion : public Escena
	{
	public:
		Puntuacion();
		static int indiceJugadorConCartaMayor = 0;
		void timerTick(System::Object^  sender, System::EventArgs^  e);
		void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
		void mostrarInfoJuego(Graphics^ graphics);
	};
}