#pragma once
#include "CartaCambioSentido.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	CartaCambioSentido::CartaCambioSentido() : CartaEspecial() { ; }

	void CartaCambioSentido::aplicarEfecto()
	{
		if (WinForm::ordenTurnos == OrdenTurnos::ASCENDENTE) {
			WinForm::ordenTurnos = OrdenTurnos::DESCENDETE;
		}
		else if (WinForm::ordenTurnos == OrdenTurnos::DESCENDETE) {
			WinForm::ordenTurnos = OrdenTurnos::ASCENDENTE;
		}
		Juego::cambiarTurno();
	};
}