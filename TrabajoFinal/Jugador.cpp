#include "WinForm.h"

namespace TrabajoFinal
{
	Jugador::Jugador()
	{
		this->mano = gcnew List<Carta^>();
	}

	void Jugador::sacarDeBaraja()
	{
		Carta ^ultimaCarta = WinForm::mazo[0];
		WinForm::mazo->Remove(ultimaCarta);
		this->mano->Add(ultimaCarta);
	}

}