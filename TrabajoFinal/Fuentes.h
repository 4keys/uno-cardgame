#pragma once
using namespace System::Drawing;

namespace TrabajoFinal
{
	public ref class Fuentes {
		public:
			Fuentes();

			static Font^ GRANDE;
			static Font^ GRANDE_GRUESA;
			static Font^ MEDIANA;
			static Font^ MEDIANA_GRUESA;
			static Font^ PEQUENIA;
	};
}
