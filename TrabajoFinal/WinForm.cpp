#include "WinForm.h"

namespace TrabajoFinal
{
	WinForm::WinForm(void)
	{
		InitializeComponent();
		winform = this;
		aleatorio = gcnew Random();

		imagenes = gcnew Imagenes();
		fuentes = gcnew Fuentes();

		jugadores = gcnew List <Jugador^>();

		graphics = this->CreateGraphics();
		context = BufferedGraphicsManager::Current;

		inicioEscena = gcnew Inicio();
		seleccionJugadores = gcnew SeleccionJugadores();
		reparticionCartas;
		juegoEscena = gcnew Juego();
		puntuacionEscena = gcnew Puntuacion();

		Escena::ActivarEscena(inicioEscena);
	}

	WinForm::~WinForm()
	{
		if (components)
		{
			delete components;
		}
	}

	void WinForm::darCartaAJugador(Carta^ carta, Jugador^ jugador) {
		WinForm::mazo->Remove(carta);
		jugador->mano->Add(carta);
	}
}
