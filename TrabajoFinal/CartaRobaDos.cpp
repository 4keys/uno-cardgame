#pragma once
#include "CartaRobaDos.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	CartaRobaDos::CartaRobaDos() : CartaEspecial() { ; }

	void CartaRobaDos::aplicarEfecto() {
		int siguienteTurno = Juego::obtenerSiguienteTurno();

		Jugador ^jugadorAfectado = WinForm::jugadores[siguienteTurno];
		jugadorAfectado->sacarDeBaraja();
		jugadorAfectado->sacarDeBaraja();

		int siguienteTurnoDelSiguienteTurno = Juego::obtenerSiguienteTurno(siguienteTurno);
		Juego::cambiarTurnoATurno(siguienteTurnoDelSiguienteTurno);
	};
}