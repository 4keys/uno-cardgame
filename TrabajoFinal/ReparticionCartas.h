#pragma once
#include "Escena.h"
#include "Fuentes.h"
#include "Imagenes.h"

namespace TrabajoFinal
{
	public ref class ReparticionCartas : public Escena
	{
		public:
			ReparticionCartas();
			static int indiceJugadorConCartaMayor = 0;
			void timerTick(System::Object^  sender, System::EventArgs^  e);
			void teclaDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
			void inicializarMazo();
			void barajarMazo();
			void obtenerPrimerJugador();
			void mostrarInfoJuego(Graphics^ graphics);
			void repartirCartas();
	};
}