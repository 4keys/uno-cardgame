#pragma once
#include "CartaRobaCuatro.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	CartaRobaCuatro::CartaRobaCuatro() : CartaEspecial() { ; }

	void CartaRobaCuatro::aplicarEfecto() {
		Colores nuevoColor;
		nuevoColor = Juego::nuevoColorComodin;

		if (Juego::jugadorEnTurno != WinForm::yo) {
			Colores colorAleatorio = (Colores)WinForm::aleatorio->Next(4);
			nuevoColor = colorAleatorio;
		}

		int siguienteTurno = Juego::obtenerSiguienteTurno();
		Jugador ^jugadorAfectado = WinForm::jugadores[siguienteTurno];
		jugadorAfectado->sacarDeBaraja();
		jugadorAfectado->sacarDeBaraja();
		jugadorAfectado->sacarDeBaraja();
		jugadorAfectado->sacarDeBaraja();

		int siguienteTurnoDelSiguienteTurno = Juego::obtenerSiguienteTurno(siguienteTurno);
		Juego::nuevoColorComodin = nuevoColor;
		Juego::cambiarTurnoATurno(siguienteTurnoDelSiguienteTurno);
	};
}