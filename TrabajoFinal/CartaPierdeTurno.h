#pragma once
#include "CartaEspecial.h"

namespace TrabajoFinal
{
	public ref class CartaPierdeTurno : public CartaEspecial
	{
	public:
		CartaPierdeTurno();
		void aplicarEfecto() override;
	};
}