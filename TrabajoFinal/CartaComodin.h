#pragma once
#include "CartaEspecial.h"

namespace TrabajoFinal
{
	public ref class CartaComodin : public CartaEspecial
	{
	public:
		CartaComodin();
		void aplicarEfecto() override;
	};
}