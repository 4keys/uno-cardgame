#pragma once
#include "Carta.h"

namespace TrabajoFinal
{
	public ref class CartaEspecial abstract : public Carta
	{
	public:
		CartaEspecial() : Carta() { ; };
		virtual void aplicarEfecto() = 0;
	};
}