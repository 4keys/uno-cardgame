#pragma once

using namespace System::Drawing;

namespace TrabajoFinal
{
	public ref class Imagenes
	{
		public:
			Imagenes();

			static Bitmap ^Inicio;
			static Bitmap ^ReparticionCartas;
			static Bitmap ^FondoDeDados;
			static Bitmap ^Campo;
			static Bitmap ^Puntuacion;
			static Bitmap ^Y0;
			static Bitmap ^Y1;
			static Bitmap ^Y2;
			static Bitmap ^Y3;
			static Bitmap ^Y4;
			static Bitmap ^Y5;
			static Bitmap ^Y6;
			static Bitmap ^Y7;
			static Bitmap ^Y8;
			static Bitmap ^Y9;
			static Bitmap ^Ym2;
			static Bitmap ^YR;
			static Bitmap ^YB;
			static Bitmap ^B0;
			static Bitmap ^B1;
			static Bitmap ^B2;
			static Bitmap ^B3;
			static Bitmap ^B4;
			static Bitmap ^B5;
			static Bitmap ^B6;
			static Bitmap ^B7;
			static Bitmap ^B8;
			static Bitmap ^B9;
			static Bitmap ^Bm2;
			static Bitmap ^BR;
			static Bitmap ^BB;
			static Bitmap ^R0;
			static Bitmap ^R1;
			static Bitmap ^R2;
			static Bitmap ^R3;
			static Bitmap ^R4;
			static Bitmap ^R5;
			static Bitmap ^R6;
			static Bitmap ^R7;
			static Bitmap ^R8;
			static Bitmap ^R9;
			static Bitmap ^Rm2;
			static Bitmap ^RR;
			static Bitmap ^RB;
			static Bitmap ^G0;
			static Bitmap ^G1;
			static Bitmap ^G2;
			static Bitmap ^G3;
			static Bitmap ^G4;
			static Bitmap ^G5;
			static Bitmap ^G6;
			static Bitmap ^G7;
			static Bitmap ^G8;
			static Bitmap ^G9;
			static Bitmap ^Gm2;
			static Bitmap ^GR;
			static Bitmap ^GB;
			static Bitmap ^m4;
			static Bitmap ^cC;
		};
}