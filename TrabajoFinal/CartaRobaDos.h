#pragma once
#include "CartaEspecial.h"

namespace TrabajoFinal
{
	public ref class CartaRobaDos : public CartaEspecial
	{
	public:
		CartaRobaDos();
		void aplicarEfecto() override;
	};
}