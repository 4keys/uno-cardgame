#pragma once
#include "CartaComodin.h"
#include "WinForm.h"

namespace TrabajoFinal
{
	CartaComodin::CartaComodin() : CartaEspecial() { ; }

	void CartaComodin::aplicarEfecto() {
		Colores nuevoColor;
		nuevoColor = Juego::nuevoColorComodin;

		if (Juego::jugadorEnTurno != WinForm::yo) {
			Colores colorAleatorio = (Colores) WinForm::aleatorio->Next(4);
			nuevoColor = colorAleatorio;
		}

		Juego::nuevoColorComodin = nuevoColor;
		Juego::cambiarTurno();
	};
}